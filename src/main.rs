use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::str::FromStr;

fn main() -> std::io::Result<()> {
    let file = File::open("input.txt")?;
    let buf_reader = BufReader::new(file);
    let lines = buf_reader.lines();

    let mut sums = Vec::<u32>::new();
    let mut sum = 0;

    for line in lines.flatten() {
        let line_str = line.as_str();
        match line_str {
            "" => {
                // println!("set {} total: {}", sums.len(), sum);
                sums.push(sum);
                sum = 0;
            }
            _ => {
                // println!("{}", line);
                if let Ok(line_u32) = u32::from_str(line_str) {
                    sum += line_u32;
                }
            }
        }
    }

    sums.sort_unstable();
    if let Some(last) = sums.last() {
        println!("largest {}:", last);
    } else {
        println!("could not determine largest");
        return Ok(());
    }

    let top_3_sum: u32 = sums.iter().rev().take(3).sum();
    println!("sum of top 3: {}", top_3_sum);

    Ok(())
}
